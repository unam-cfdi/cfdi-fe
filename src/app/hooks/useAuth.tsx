"use client"

import { useEffect, useState } from "react"
import { validateToken } from "../services/authService"

export interface AuthState {
  token: string
}

export function useAuth() {
  const [isLoading, setIsLoading] = useState(true)
  const [auth, setAuth] = useState<AuthState | null>(null)

  const getVerifiedToken: () => void = async () => {
    const storage = localStorage.getItem('auth')

    if (storage === null) {
      setIsLoading(false);
    } else {

      const state: AuthState = JSON.parse(localStorage.getItem('auth') || '{}')
      const token = state.token

      const verifiedToken = await validateToken(token, state)

      setIsLoading(false);
      if (verifiedToken) setAuth(state)
    }

  }

  useEffect(() => {
    getVerifiedToken()
  }, [])

  return {
    auth,
    setAuth,
    isLoading
  }
}

