import React, { useState } from 'react'
import Link from 'next/link'

const menuItems = [
  { path: '/', label: 'Home' },
  { path: '/login', label: 'Login' },
  { path: '/contact', label: 'Contact' },
  // Add more menu items here
];

const SideMenu = () => {

  return (
    <div className={`fixed top-0 left-0 w-64 h-screen bg-gray-800 text-white z-20`}>
      <div className="flex items-center justify-between px-4 py-2">
        <h2 className="text-xl font-bold">Sistema de Facturación</h2>
      </div>
      <nav className="mt-4">
        {menuItems.map((item) => (
          <Link key={item.path} href={item.path} className="text-gray-300 hover:text-gray-200 px-4 py-2 flex items-center">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              {/* Replace with custom icons based on menu items */}
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 11a6 6 0 0 1 12 0v4a6 6 0 0 1-6 6H3zM3 17a6 6 0 0 1 6 6v4a6 6 0 0 1-6 6H3z" />
            </svg>
            {item.label}
          </Link>
        ))}
      </nav>
    </div>
  );
};

export default SideMenu;
