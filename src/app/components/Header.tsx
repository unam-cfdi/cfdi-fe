import Image from "next/image"

const Header = () => {
  return (
    <header className="bg-pink-800 text-white p-4 ml-64 flex items-center justify-stretch">
      <div className="mx-10">
        <a href="#" className="-m-1.5 p-1.5">
            <Image
              width={50}
              height={50}
              src="/farmacia.png" alt=""/>
        </a>
      </div>
      <h1 className="text-xl">Farmacia de la FI</h1>
    </header>
  )
}

export default Header