const MainContent = ({ children }: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <main className="container pl-24 py-9 ml-64 mr-24">
      {children}
    </main>
  )
}

export default MainContent