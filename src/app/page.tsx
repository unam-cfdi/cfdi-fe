"use client"

import Header from "./components/Header";
import MainContent from "./components/MainContent";
import SideMenu from "./components/SideMenu";

export default function Home() {
  return (
    <>
    <Header/>
    <MainContent>
      <SideMenu/>
      <h1>Home</h1>
    </MainContent>
    </>
  );
}
