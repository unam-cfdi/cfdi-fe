import { createApiGetRequest, createApiPostRequest } from "./requestService";

type AuthState = {
  readonly token: string;
}

type LoginRequest = {
  readonly email: string;
  readonly password: string
}

type ErrorHandler = (error: any) => void

export const getStoredAuthInfo = () => {
  const storage = localStorage.getItem('auth') || ''
  return <AuthState>JSON.parse(storage)
}

export const storeAuthInfo = (info: AuthState) => {
  const storage = JSON.stringify(info)
  localStorage.setItem('auth', storage)
}

export const requestLogin = async (request: LoginRequest) : Promise<AuthState> => {
  const response = await createApiPostRequest('/auth/login', request);

  if (!response.ok) {
    throw new Error('Invalid credentials')
  }

  const data = <AuthState> await response.json();
  storeAuthInfo(data)
  // Handle successful login (e.g., redirect to dashboard)
  return data;
}

export const validateToken = async (token: string, auth: AuthState) : Promise<boolean> => {
  const response = await createApiGetRequest('/auth', auth);

  return response.ok
}